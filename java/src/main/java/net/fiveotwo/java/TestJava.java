package net.fiveotwo.java;

import playn.core.PlayN;
import playn.java.JavaPlatform;

import net.fiveotwo.core.Test;

public class TestJava {

  public static void main(String[] args) {
    JavaPlatform.register();
    PlayN.run(new Test());
  }
}
