package net.fiveotwo.android;

import playn.android.GameActivity;
import playn.core.PlayN;

import net.fiveotwo.core.Test;

public class TestActivity extends GameActivity {

  @Override
  public void main(){
    PlayN.run(new Test());
  }
}
